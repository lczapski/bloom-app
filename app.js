/**
 * Copyright 2015 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

var express = require('express'); // app server
var bodyParser = require('body-parser'); // parser for post requests
var AssistantV1 = require('watson-developer-cloud/assistant/v1'); // watson sdk
var jwt = require('jsonwebtoken');
const fileUpload = require('express-fileupload');
var crypto = require('crypto');
var transliteration = require('transliteration');
var tr = transliteration.transliterate

var app = express();


var addHeaders = function(req,res,next){
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-type,Accept,X-Custom-Header,authorization");
  
  if (req.method === "OPTIONS") {
    res.header('Access-Control-Allow-Origin', req.headers.origin);
  } else {
    res.header('Access-Control-Allow-Origin', '*');
  }

  if (req.method === "OPTIONS") {
      return res.status(200).end();
  }
  next(); // http://expressjs.com/guide.html#passing-route control
};

app.get('/*',addHeaders);
app.post('/*',addHeaders);
app.options('/*',addHeaders);

var db = require('./db');

// Bootstrap application settings
app.use(express.static('./public')); // load UI from public folder
app.use(bodyParser.json());
app.use(fileUpload());

// Create the service wrapper

var assistant = new AssistantV1({
  version: '2018-07-10'
});

db.initDBConnection();


app.get('/api/ping', function(req, res) {
  var data = verifyToken(req, res);
  console.log(data);

  db.simpleInsert({
            name: "name1",
            value: "value1"
        }, function(err, doc) {
            if (err) {
                console.log(err);
                res.sendStatus(500);
            } else
                res.send('Insert Express!');
            res.end();
        });
 });

app.get('/api/image/:uid',function(req,res,next) {
  console.log('Displaying information for uid ' + req.params.uid);

  db.getImg(req.params.uid, function(data){
    var img = data;
    res.writeHead(200, {
        'Content-Type': img.mimetype,
        'Content-disposition': 'attachment;filename=' + tr(img.name),
        'Content-Length': img.data.data.length
    });
    res.end(new Buffer(img.data.data, 'binary'));

  });
});

app.post('/api/login', function(req, res) {
  console.log(req.body.login);

  db.getUser(req.body.login, function (err, data) {
    if (err) {
      return res.status(err.code || 500).json(err);
    }
    if (data) {
      var hash = crypto.createHash('sha256').update(req.body.password).digest('base64');
      console.log("----");
      console.log(hash);
      console.log("----");
      if (hash === data.password) {
        var token = createToken(data);
        // console.log(token);
        // console.log(jwt.decode(token));
        return res.json({
          login:data.login, 
          name:data.name,
          token:token
        });
      }
    }
    return res.status(404).end();
  });

});

app.get('/api/chats', function(req, res) {

  var data = verifyToken(req, res);
  var loginUser = (data) ? data.login : null;

  db.listUserChat(loginUser, function (err, data) {
    if (err) {
      return res.status(err.code || 500).json(err);
    }
    if (data) {
      res.send(data);
    }
    return res.status(404).end();
  });

});

app.get('/api/chats/:id',function(req,res,next) {

  var data = verifyToken(req, res);
  var loginUser = (data) ? data.login : null;

  db.getUserChat(loginUser, req.params.id, function (err, data) {
    if (err) {
      return res.status(err.code || 500).json(err);
    }
    if (data) {
      res.send(data);
    }
    return res.status(404).end();
  });

});

function createToken(data) {
  return jwt.sign({
    exp: Math.floor(Date.now() / 1000) + (12 * 60 * 60),
    id: data._id,
    login: data.login
  }, 'secret');
}

function verifyToken(req, res) {
  try {
    var authorization = req.header("authorization");
    if (!authorization) {
      return null;
    }
    var arr = authorization.split(" ");
    var token = arr[arr.length - 1];
    console.log(arr);
    return jwt.verify(token, 'secret');
  } catch(err) {
    return null;
  }
}

app.post('/upload', function(req, res) {

  uploadImage(req, res, function(doc) {
    console.log('Insert Image: '+ doc.fileId);
    res.send({fileId:doc.fileId});
    res.end();
  });

});

app.post('/api/upload', function(req, res) {

  uploadImage(req, res, function(doc) {
    console.log('Insert Image: '+ doc.fileId);
    res.send({fileId:doc.fileId});
    res.end();
  });

});

function uploadImage(req, res, onSuccess) {
  // Uploaded files:
  console.log(req.files.sampleFile.name);
  console.log(req.body.conversation_id);

  db.addImg(req.body.conversation_id, {
    name: req.files.sampleFile.name,
    data: req.files.sampleFile.data,
    encoding: req.files.sampleFile.encoding,
    mimetype: req.files.sampleFile.mimetype
  }, function(err, doc) {
        if (err) {
            console.log(err);
            res.sendStatus(500);
            res.end();
        } else {
          onSuccess(doc);
        }
    });
}

// Endpoint to be call from the client side
app.post('/api/message', function (req, res) {
  addMessageWithImageId(req, res, null);
});

app.post('/api/chats/message', function (req, res) {
  if (req.files.sampleFile) {
    uploadImage(req, res, function(doc) {
      addMessageWithImageId(req, res, doc.fileId);
    });
  } else {
    addMessageWithImageId(req, res, null);
  }
  
});

function addMessageWithImageId(req, res, fileId) {

  var workspace = getWorkspace();
  if(!workspace) {
    return;
  }

  var data = verifyToken(req, res);
  var loginUser = (data) ? data.login : null;

  var input = (req.body.input) ? req.body.input : (req.body.text) ? {text:req.body.text} : {} || {}

  var inputeDate = Date.now();

  var payload = {
    workspace_id: workspace,
    context: req.body.context || {},
    input: input,
    title: req.body.title,
    fileId: fileId
  };

  if (req.body.conversation_id) {
    db.getMsg(req.body.conversation_id, function(data){
      payload.context = data.context;
      payload.title = data.title;
      sendToAssistant(res, payload, loginUser, inputeDate);
    }, function() {res.status(404).end();});
  } else {
    sendToAssistant(res, payload, loginUser, inputeDate);
  }
}

function sendToAssistant(res, payload, loginUser, inputeDate) {
      // Send the input to the assistant service
      assistant.message(payload, function (err, data) {
        if (err) {
          return res.status(err.code || 500).json(err);
        }
    
        return res.json(updateMessage(payload, data, loginUser, inputeDate, Date.now()));
      });
}

function getWorkspace() {
  var workspace = process.env.WORKSPACE_ID || '<workspace-id>';
  if (!workspace || workspace === '<workspace-id>') {
    res.json({
      'output': {
        'text': 'The app has not been configured with a <b>WORKSPACE_ID</b> environment variable. Please refer to the ' + '<a href="https://github.com/watson-developer-cloud/assistant-simple">README</a> documentation on how to set this variable. <br>' + 'Once a workspace has been defined the intents may be imported from ' + '<a href="https://github.com/watson-developer-cloud/assistant-simple/blob/master/training/car_workspace.json">here</a> in order to get a working application.'
      }
    });
    return null;
  }
  return workspace;
}

/**
 * Updates the response text using the intent confidence
 * @param  {Object} input The request to the Assistant service
 * @param  {Object} response The response from the Assistant service
 * @return {Object}          The response with the updated message
 */
function updateMessage(input, response, loginUser, inputeDate, responseDate) {
  var responseText = null;
  if (!response.output) {
    response.output = {};
  } else {

    if (response.intents.length > 0 && (response.intents[0].intent === 'add' ||
      response.intents[0].intent === 'multiply')) {
      response = getCalculationResult(response);
    }
    
    var id = response.context.conversation_id;

    if (id) {

      db.updateMsg(id, input, response, loginUser, inputeDate, responseDate);

      if (response.output.action == 'save') {
        db.insertPatient({
          name: response.context.name,
          sex: response.context.sex,
          dateOfBirth: response.context.dateOfBirth,
          dysfunctions: response.context.dysfunctions
        }, function(err, doc) {
            if (err) {
                console.log(err);
            } else
              console.log('insertPatient');
        });
      }

    }

    response.conversation_id = response.context.conversation_id;
    response.text = response.output.text.join('\n');
    response.fileId = input.fileId;
    response.title = input.title;
    
    return response;
  }
  if (response.intents && response.intents[0]) {
    var intent = response.intents[0];
    // Depending on the confidence of the response the app can return different messages.
    // The confidence will vary depending on how well the system is trained. The service will always try to assign
    // a class/intent to the input. If the confidence is low, then it suggests the service is unsure of the
    // user's intent . In these cases it is usually best to return a disambiguation message
    // ('I did not understand your intent, please rephrase your question', etc..)



    if (intent.confidence >= 0.75) {
      responseText = 'I understood your intent was ' + intent.intent;
    } else if (intent.confidence >= 0.5) {
      responseText = 'I think your intent was ' + intent.intent;
    } else {
      responseText = 'I did not understand your intent';
    }
  }
  response.output.text = responseText;

  return response;
}


/**
* Get the operands, perform the calculation and update the response text based on the
* calculation.
* @param {Object} response The response from the Conversation service
* @return {Object} The response with the updated message
*/
function getCalculationResult(response) {
  //An array holding the operands
  var numbersArr = [];
  //Fill the content of the array with the entities of type 'sys-number'
  for (var i = 0; i < response.entities.length; i++) {
    if (response.entities[i].entity === 'sys-number') {
      numbersArr.push(response.entities[i].value);
    }
  }
  // In case the user intent is add, perform the addition
  // In case the intent is multiply, perform the multiplication
  var result = 0;
  if (response.intents[0].intent === 'add') {
    result = parseInt(numbersArr[0]) + parseInt(numbersArr[1]);
  } else if (response.intents[0].intent === 'multiply') {
    result = parseInt(numbersArr[0]) * parseInt(numbersArr[1]);
  }
  // Replace _result_ in Conversation Service response, with the actual calculated result
  var output = response.output.text[0];
  output = output.replace('_result_', result);
  response.output.text[0] = output;
  response.output.generic[0].text = output;
  // Return the updated response text based on the calculation
  return response;
}

module.exports = app;
// Create the service wrapper
var fs = require('fs');

var db;

const uuidv1 = require('uuid/v1');

var cloudant;
var dbCredentials = {
    dbName: 'test'
};

getDBCredentialsUrl = function(jsonData) {
    var vcapServices = JSON.parse(jsonData);
    // Pattern match to find the first instance of a Cloudant service in
    // VCAP_SERVICES. If you know your service key, you can access the
    // service credentials directly by using the vcapServices object.
    for (var vcapService in vcapServices) {
        if (vcapService.match(/cloudant/i)) {
            return vcapServices[vcapService][0].credentials.url;
        }
    }
}

exports.simpleInsert = function(value, callback) {
    db.insert(value, '', callback);
}

exports.insertPatient = function(value, callback) {
    db.insert(value, '', callback);
}

insertMsg = function(id, msg, context, title, addToMsg, loginUser) {
    var arr = [];
    arr.push(msg)
    db.insert({
      msgs: arr,
      text: [addToMsg[1]],
      title: title,
      createTime: Date.now(),
      type: "chat",
      loginUser: loginUser,
      context: context
      }
      , id, function(err, doc) {
        if (err) {
            console.log(err);
        } 
        console.log('insert');
    });
}

updateMsg = function(doc, callBack = null) {
    db.insert(doc, function(err, doc) {
        if (callBack) {
            callBack(err, doc);
        } else {
            if (err) {
                console.log(err);
            } 
            console.log('update');
        }
    });
}

exports.updateMsg = function(id, input, response, loginUser, inputeDate, responseDate) {
    var msg = {
        input: input,
        response: response
      }
    var addToMsg = [
        {text:input.input.text, 
            time:inputeDate,
            fileId:input.fileId,
            id:loginUser}, 
        {text:response.output.text.join('\n'), 
            time:responseDate,
            id:"watson"}]

    var context = response.context;
    var title = input.title;
    console.log('updateMsg: '+id);
    _getMsg(id, function(doc) {
        doc.msgs.push(msg);
        doc.context = context;
        doc.loginUser = (doc.loginUser) ? doc.loginUser : loginUser;
        for (var i = 0; i < addToMsg.length; i++) {
            doc.text.push(addToMsg[i]);
        }
        
        updateMsg(doc);
    }, function() {
        insertMsg(id, msg, context, title, addToMsg, loginUser);
    });
}

_getMsg = function(id, callBack, errorCallBack) {
    console.log('_getMsg: '+id);
    db.get(id, {
        revs_info: true
    }, function(err, doc) {
        if (!err) {
          if (!doc) {
            errorCallBack();
          } else {
            callBack(doc);
          }
        } else {
          if (!doc) {
            errorCallBack();
          }
        }
    });
}

_getUser = function(login, callBack, errorCallBack) {
    db.find({selector:{type:'user',login:login}}, function(er, result) {
        if (er) {
            callBack(er, null);
        }
      
        console.log('Found %d documents with type user, login: %s', result.docs.length, login);
        if ( result.docs.length > 0) {
          callBack(null, result.docs[0]);
        } else {
            callBack(null, null);
        }

      });
}

exports.getUser = function(login, callBack) {
    console.log('getUser: '+login);
    _getUser(login, callBack);
}

_listUserChat = function(login, callBack, errorCallBack) {
    db.find({selector:{type:'chat',loginUser:login}
    ,fields:[
        'title',
        'createTime',
        '_id'
    ]}, function(er, result) {
        if (er) {
            callBack(er, null);
        }
      
        console.log('Found %d documents with type user, login: %s', result.docs.length, login);
        if ( result.docs.length > 0) {
            for(i = result.docs.length; i--;){
                result.docs[i].id = result.docs[i]._id;
                result.docs[0].createTime = new Date(result.docs[0].createTime);
                delete result.docs[i]._id;
            }
            callBack(null, result.docs);
        } else {
            callBack(null, null);
        }

      });
}

exports.listUserChat = function(login, callBack) {
    console.log('listUserChat: '+login);
    _listUserChat(login, callBack);
}

_getUserChat = function(login, id, callBack, errorCallBack) {
    db.find({selector:{type:'chat',loginUser:login,_id:id}
    ,fields:[
        'title',
        'createTime',
        'text',
        '_id'
    ]}, function(er, result) {
        if (er) {
            callBack(er, null);
        }
      
        console.log('Found %d documents with type user, login: %s', result.docs.length, login);
        if ( result.docs.length > 0) {
            result.docs[0].id = result.docs[0]._id;
            result.docs[0].createTime = new Date(result.docs[0].createTime);
            delete result.docs[0]._id;
            callBack(null, result.docs[0]);
        } else {
            callBack(null, null);
        }

      });
}

exports.getUserChat = function(login, id, callBack) {
    console.log('getUserChat: '+id);
    _getUserChat(login, id, callBack);
}

exports.getMsg = function(id, callBack, callBackError = null) {
    _getMsg(id, function(doc) {
        callBack(doc);
    }, function() {
        if (callBackError) {
            callBackError();
        }
    });
}


exports.addImg = function(id, file, callBack) {
    _getMsg(id, function(doc) {
        doc.fileId = uuidv1();
        file.type = "file";
        db.insert(file, doc.fileId, function(img) { 
            updateMsg(doc, function(err, doc1) {
                if (callBack) {
                    callBack(err, doc);
                } else {
                    if (err) {
                        console.log(err);
                    } 
                    console.log('update');
                }
            });
        });
    }, function() {});
}

exports.getImg = function(id, callBack, callBackError = null) {
    _getImg(id, function(doc) {
        callBack(doc);
    }, function() {
        if (callBackError) {
            callBackError();
        }
    });
}

_getImg = function(id, callBack, errorCallBack) {
    console.log('_getImg: '+id);
    db.get(id, {
        revs_info: true
    }, function(err, doc) {
        if (!err) {
          if (!doc) {
            errorCallBack();
          } else {
            callBack(doc);
          }
        } else {
          if (!doc) {
            errorCallBack();
          }
        }
    });
}

exports.initDBConnection = function() {
    //When running on Bluemix, this variable will be set to a json object
    //containing all the service credentials of all the bound services
    if (process.env.VCAP_SERVICES) {
        dbCredentials.url = getDBCredentialsUrl(process.env.VCAP_SERVICES);
    } else { //When running locally, the VCAP_SERVICES will not be set

        // When running this app locally you can get your Cloudant credentials
        // from Bluemix (VCAP_SERVICES in "cf env" output or the Environment
        // Variables section for an app in the Bluemix console dashboard).
        // Once you have the credentials, paste them into a file called vcap-local.json.
        // Alternately you could point to a local database here instead of a
        // Bluemix service.
        // url will be in this format: https://username:password@xxxxxxxxx-bluemix.cloudant.com
        dbCredentials.url = getDBCredentialsUrl(fs.readFileSync("vcap-local.json", "utf-8"));
    }

    cloudant = require('cloudant')(dbCredentials.url);

    // check if DB exists if not create
    cloudant.db.create(dbCredentials.dbName, function(err, res) {
        if (err) {
            console.log('Could not create new db: ' + dbCredentials.dbName + ', it might already exist.');
        }
    });

    db = cloudant.use(dbCredentials.dbName);
};
